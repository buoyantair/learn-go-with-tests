# Learning Golang with TDD

I have started learning Golang with TDD thanks to this [🎊amazing book🎉](https://quii.gitbook.io/learn-go-with-tests)

Feel free to go through the exercises and practice I have made with golang and TDD 🙌